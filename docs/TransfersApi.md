# \TransfersApi

All URIs are relative to *http://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**complete_transfer**](TransfersApi.md#complete_transfer) | **POST** /accounts/{accountId}/transfers/{transferId}/complete | Complete a transfer
[**create_transfer**](TransfersApi.md#create_transfer) | **POST** /accounts/{accountId}/transfers | Create a transfer
[**delete_transfer**](TransfersApi.md#delete_transfer) | **DELETE** /accounts/{accountId}/transfers/{transferId} | Delete a transfer
[**get_transfer**](TransfersApi.md#get_transfer) | **GET** /accounts/{accountId}/transfers/{transferId} | Retrieve information for a specific transfer
[**list_transfers**](TransfersApi.md#list_transfers) | **GET** /accounts/{accountId}/transfers | List all transfers related to the account
[**update_transfer**](TransfersApi.md#update_transfer) | **PUT** /accounts/{accountId}/transfers/{transferId} | Change transfer information



## complete_transfer

> crate::models::Transfer complete_transfer(account_id, transfer_id)
Complete a transfer

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**account_id** | **String** | The id of the account | [required] |
**transfer_id** | **String** | The id of the transfer to operate on | [required] |

### Return type

[**crate::models::Transfer**](Transfer.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## create_transfer

> crate::models::Transfer create_transfer(account_id, create_transfer_request)
Create a transfer

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**account_id** | **String** | The id of the account | [required] |
**create_transfer_request** | Option<[**CreateTransferRequest**](CreateTransferRequest.md)> |  |  |

### Return type

[**crate::models::Transfer**](Transfer.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## delete_transfer

> crate::models::Transfer delete_transfer(account_id, transfer_id)
Delete a transfer

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**account_id** | **String** | The id of the account | [required] |
**transfer_id** | **String** | The id of the transfers to operate on | [required] |

### Return type

[**crate::models::Transfer**](Transfer.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## get_transfer

> crate::models::Transfer get_transfer(account_id, transfer_id)
Retrieve information for a specific transfer

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**account_id** | **String** | The id of the account | [required] |
**transfer_id** | **String** | The id of the transfers to operate on | [required] |

### Return type

[**crate::models::Transfer**](Transfer.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## list_transfers

> Vec<crate::models::Transfer> list_transfers(account_id)
List all transfers related to the account

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**account_id** | **String** | The id of the account | [required] |

### Return type

[**Vec<crate::models::Transfer>**](Transfer.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## update_transfer

> crate::models::Transfer update_transfer(account_id, transfer_id, update_transfer_request)
Change transfer information

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**account_id** | **String** | The id of the account | [required] |
**transfer_id** | **String** | The id of the transfers to operate on | [required] |
**update_transfer_request** | Option<[**UpdateTransferRequest**](UpdateTransferRequest.md)> |  |  |

### Return type

[**crate::models::Transfer**](Transfer.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

