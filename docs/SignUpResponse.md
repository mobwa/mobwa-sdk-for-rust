# SignUpResponse

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**username** | Option<**String**> |  | [optional]
**email** | Option<**String**> |  | [optional]
**role** | Option<**String**> |  | [optional]
**accounts** | Option<[**Vec<crate::models::Account>**](Account.md)> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


