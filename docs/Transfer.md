# Transfer

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | Option<**String**> |  | [optional]
**message** | Option<**String**> |  | [optional]
**amount** | Option<**i32**> |  | [optional]
**currency** | Option<**String**> |  | [optional]
**created_at** | Option<**String**> |  | [optional]
**updated_at** | Option<**String**> |  | [optional]
**status** | Option<**String**> |  | [optional]
**auto_complete** | Option<**bool**> |  | [optional]
**source** | Option<[**crate::models::TransferSource**](Transfer_source.md)> |  | [optional]
**destination** | Option<[**crate::models::TransferSource**](Transfer_source.md)> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


