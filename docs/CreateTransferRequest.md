# CreateTransferRequest

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**message** | Option<**String**> |  | [optional]
**amount** | Option<**i32**> |  | [optional]
**currency** | Option<**String**> |  | [optional]
**auto_complete** | Option<**bool**> |  | [optional]
**destination** | Option<**String**> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


