# \AccountsApi

All URIs are relative to *http://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**create_account**](AccountsApi.md#create_account) | **POST** /accounts | Create an account
[**get_account**](AccountsApi.md#get_account) | **GET** /accounts/{accountId} | Get account information
[**list_account**](AccountsApi.md#list_account) | **GET** /accounts | List all accounts
[**recharge_account**](AccountsApi.md#recharge_account) | **POST** /accounts/{accountId}/recharge | Recharge the account
[**update_account**](AccountsApi.md#update_account) | **PUT** /accounts/{accountId} | Update account information
[**withdraw_money**](AccountsApi.md#withdraw_money) | **POST** /accounts/{accountId}/withdraw | Withdraw money from the account



## create_account

> Vec<crate::models::Transfer> create_account(create_account_request)
Create an account

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**create_account_request** | Option<[**CreateAccountRequest**](CreateAccountRequest.md)> |  |  |

### Return type

[**Vec<crate::models::Transfer>**](Transfer.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## get_account

> crate::models::Account get_account(account_id)
Get account information

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**account_id** | **String** | The id of the account | [required] |

### Return type

[**crate::models::Account**](Account.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## list_account

> Vec<crate::models::Account> list_account()
List all accounts

### Parameters

This endpoint does not need any parameter.

### Return type

[**Vec<crate::models::Account>**](Account.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## recharge_account

> crate::models::Account recharge_account(account_id, recharge_account_request)
Recharge the account

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**account_id** | **String** | The id of the account | [required] |
**recharge_account_request** | Option<[**RechargeAccountRequest**](RechargeAccountRequest.md)> |  |  |

### Return type

[**crate::models::Account**](Account.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## update_account

> crate::models::Account update_account(account_id, update_account_request)
Update account information

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**account_id** | **String** | The id of the account | [required] |
**update_account_request** | Option<[**UpdateAccountRequest**](UpdateAccountRequest.md)> |  |  |

### Return type

[**crate::models::Account**](Account.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## withdraw_money

> crate::models::Account withdraw_money(account_id, body)
Withdraw money from the account

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**account_id** | **String** | The id of the account | [required] |
**body** | Option<**crate::models::RechargeAccountRequest**> |  |  |

### Return type

[**crate::models::Account**](Account.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

