# \UsersApi

All URIs are relative to *http://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**create_user**](UsersApi.md#create_user) | **POST** /users | Create a user
[**get_user**](UsersApi.md#get_user) | **GET** /users/{userId} | Get user information
[**list_users**](UsersApi.md#list_users) | **GET** /users | List all users
[**update_user**](UsersApi.md#update_user) | **PUT** /users/{userId} | Update user information



## create_user

> Vec<crate::models::Transfer> create_user(create_user_request)
Create a user

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**create_user_request** | Option<[**CreateUserRequest**](CreateUserRequest.md)> |  |  |

### Return type

[**Vec<crate::models::Transfer>**](Transfer.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## get_user

> crate::models::Account get_user(user_id)
Get user information

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**user_id** | **String** | The id of the user to operate on | [required] |

### Return type

[**crate::models::Account**](Account.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## list_users

> Vec<crate::models::User> list_users()
List all users

### Parameters

This endpoint does not need any parameter.

### Return type

[**Vec<crate::models::User>**](User.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## update_user

> crate::models::Account update_user(user_id, update_account_request)
Update user information

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**user_id** | **String** | The id of the user to operate on | [required] |
**update_account_request** | Option<[**UpdateAccountRequest**](UpdateAccountRequest.md)> |  |  |

### Return type

[**crate::models::Account**](Account.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

